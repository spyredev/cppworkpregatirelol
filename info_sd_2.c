#include <stdio.h>
#include <string.h>
#include <malloc.h>

struct Vehicul{
    char * marca;
    char * model;
    
};


int suma(int x){
    return x+10;
}


struct Vehicul * creareVehicul(char * model, char * marca){
    struct Vehicul * nou = (struct Vehicul *)malloc(sizeof(struct Vehicul));
    nou->marca = (char *)malloc(sizeof(char) * strlen(marca)+1);
    strcpy(nou->marca, marca);
    nou->model = (char *)malloc(sizeof(char) * strlen(model)+1);
    strcpy(nou->model, model);
    return nou;
}


void afisareVehicul(struct Vehicul * veh){
    printf("Marca: %s, Model: %s\n", veh->marca, veh->model);
}

int main(){


    struct Vehicul v1;
    struct Vehicul * pointerLaVehicul = &v1;
    
    v1.marca = (char *)malloc(sizeof(char) * 300);
    v1.model = (char *)malloc(sizeof(char) * 400);
    strcpy(v1.marca, "Porsche");
    strcpy(v1.model, "Carrera");
    
    printf("Marca: %s, model: %s\n", v1.marca, v1.model);
    
    
    struct Vehicul * v2 = (struct Vehicul *)malloc(sizeof(struct Vehicul)); // un malloc pt v2 
    v2->marca = (char *)malloc(sizeof(char) * 300);
    (*v2).model = (char *)malloc(sizeof(char) * 300);
    strcpy(v2->marca, "Audi");
    strcpy(v2->model, "TT");
    
    printf("Marca: %s, model: %s\n", v2->marca, v2->model);
    
    struct Vehicul * v3 = creareVehicul("330", "BMW");
    afisareVehicul(v3);
    
    
    afisareVehicul(pointerLaVehicul);
    afisareVehicul(&v1); // orice element aloca static (i.e. variabila) poate fi 'inghesuita' intr-un pointer prin &
    
    
    return 0;
}