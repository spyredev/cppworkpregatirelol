#include <stdio.h>
#include <string.h>
#include <malloc.h>

struct Vehicul{
    char marca[64];
    char model[64];
    
};

int main(){


    struct Vehicul v1;
    strcpy(v1.marca, "Porsche");
    strcpy(v1.model, "Carrera");
    
    printf("Marca: %s, model: %s\n", v1.marca, v1.model);
    
    
    struct Vehicul * v2 = (struct Vehicul *)malloc(sizeof(struct Vehicul));
    strcpy((*v2).marca, "Audi");
    strcpy((*v2).model, "TT");
    printf("Marca: %s, model: %s\n", (*v2).marca, (*v2).model);
    printf("Marca: %s, model: %s\n", v2->marca, v2->model);
    
    
    /*
    char * info = (char*)malloc(sizeof(char) * 300); // char info[300]
    char ** cuvinte;                                // char cuvinte[10][20]
    
    strcpy(info, "Pizza is awsome");
    printf("Mesajul este: %s\n", info);
    
    printf("Un caracter din mesaj: %c\n", info[0]);
    
    int * multeInt = (int *)malloc(sizeof(int) * 3);
    multeInt[0] = 10;
    multeInt[1] = 20;
    multeInt[2] = 30;
    printf("valoare: %d\n", multeInt);
    printf("valoare: %d\n", *multeInt);
    printf("valoare: %d\n", *(multeInt+30));
    
    
    int x = 20;
    int * px = &x;
    printf("Adresa este: %d\n", &x);
    printf("Valoarea este: %d\n", x);
    
    printf("Adresa este: %d\n", px);
    printf("Valoarea este: %d\n", *px);
    
    */
    
    
    
    return 0;
}