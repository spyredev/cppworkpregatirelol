#include <stdio.h>
#include <string.h>
#include <malloc.h>


struct Persoana{
    char * nume;
};


struct Nod{
    
    struct Persoana * pers;
    struct Nod * next;
};







struct Vehicul{
    char * marca;
    char * model;
    struct Persoana * sofer;
};




struct Vehicul * creareVehicul(char * model, char * marca){
    struct Vehicul * nou = (struct Vehicul *)malloc(sizeof(struct Vehicul));
    nou->marca = (char *)malloc(sizeof(char) * strlen(marca)+1);
    strcpy(nou->marca, marca);
    nou->model = (char *)malloc(sizeof(char) * strlen(model)+1);
    strcpy(nou->model, model);
    return nou;
}


struct Persoana * crearePersoana(char * nume){
    
    struct Persoana * nou = (struct Persoana *)malloc(sizeof(struct Persoana));
    nou->nume = (char*)malloc(sizeof(char)*strlen(nume)+1);
    strcpy(nou->nume, nume);
    return nou;
    
}

void parcurgereLista(struct Nod * primul){
    printf("---------------parcurgere-----3------------\n");
    while(primul != NULL){
        printf("[[NUME]]: %s\n", primul->pers->nume);
        primul = primul->next; // modificam pointer-ul, nu ce este in el
    }
    
}

void modifica(int x){
    x = x+100;
}

void modifica2(int **x){
    (*x) = (int *)malloc(sizeof(int));
    (**x) = (**x)+100;
}



struct Nod * creareNod(char * nume){
    struct Persoana * persoanaDinNod = crearePersoana(nume);
    struct Nod * nou = (struct Nod * )malloc(sizeof(struct Nod));
    nou->pers = persoanaDinNod;
    nou->next = NULL;
    return nou;
}


// adaugare la sfarsit
void adaugareElementInLista(struct Nod * primul, char * numeNou){
    while(primul->next != NULL){ // BUG!!!!
        primul = primul->next; 
    }
    // primul este la sfarsit
    primul->next = creareNod(numeNou);
}

struct Nod * adaugareLaInceput(struct Nod * primul, char * numeNou){
    struct Nod * nodNou = creareNod(numeNou);
    nodNou->next = primul;
    // primul = nodNou; // asta nu merge pentru ca modifici pointer-ul
    return nodNou;
}

int main(){
    
    int val = 30;
    int *pval =  &val;
    modifica2(&pval);
    printf("VAL : %d\n", val);
    
    struct Nod * primul =  (struct Nod *)malloc(sizeof(struct Nod));
    // primul.pers = (struct Persoana *)malloc(sizeof(struct Persoana));
    // primul.pers->nume = (char *)malloc(sizeof(char)*20+1);
    // strcpy(primul.pers->nume, "Jim");
    primul->pers = crearePersoana("Jim");
    
    struct Nod alDoilea;
    alDoilea.pers = crearePersoana("Bob");
    
    struct Nod * alTreilea = (struct Nod *)malloc(sizeof(struct Nod));
    alTreilea->pers = crearePersoana("Alice");
    
    primul->next = &alDoilea;
    printf("Nume persoana a doua: %s\n", primul->next->pers->nume);
    
    primul->next->next = alTreilea;
    printf("Nume persoana a treia: %s\n", primul->next->next->pers->nume);
    
    alTreilea->next = NULL;
    
    printf("---------------parcurgere-----------------\n");
    printf("ELement: %s\n", primul->pers->nume);
    printf("ELement: %s\n", primul->next->pers->nume);
    printf("ELement: %s\n", primul->next->next->pers->nume);
    
    printf("---------------parcurgere-----2------------\n");
    struct Nod * current = primul;
    while(current != NULL){
        printf("NUME: %s\n", current->pers->nume);
        current = current->next;
    }
    
    
    parcurgereLista(primul);
    parcurgereLista(primul);
    parcurgereLista(primul);
    parcurgereLista(primul);
    parcurgereLista(primul);
    
    
    printf("------------================LISTA=========----------------------\n\n");
    struct Nod * prim = creareNod("Ion");
    prim->next = creareNod("Vasile");
    prim->next->next = creareNod("Jackie");
    adaugareElementInLista(prim, "Satriani");
    adaugareElementInLista(prim, "Glenn");
    prim = adaugareLaInceput(prim, "Halford");
    parcurgereLista(prim);
    
    /*
    struct Vehicul * v1 = creareVehicul("Carrera", "Porsche");
    struct Persoana * p1 = crearePersoana("Jimmy");
    struct Persoana * p2 = crearePersoana("Billy");
    struct Persoana * p3 = crearePersoana("Andy");
    
    
    struct Persoana * persoane[3];
    persoane[0] = p1;
    persoane[1] = p2;
    persoane[2] = p3;
    
    
    
    v1->sofer = p1;
    
    printf("Nume sofer: %s\n", v1->sofer->nume);
    */
    return 0;
}